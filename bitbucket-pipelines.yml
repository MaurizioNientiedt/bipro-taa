image: alpine/helm:3.3.1
options:
  docker: true

definitions:
  versions:
    aws-eks-helm-deploy: &aws-eks-helm-deploy docker://andsafe/aws-eks-helm-deploy:0.6.0
  steps:
    - step: &helm-lint
        name: Helm lint
        caches:
          - docker
        script:
          - helm lint helm
    - step: &terraform-validate
        name: Terraform validate
        image: hashicorp/terraform:0.14.10
        caches:
          - docker
        script:
          - terraform init -backend=false terraform/main
          - terraform validate terraform/main
    - step: &deploy-to-development
        name: Development
        image: amazon/aws-cli
        deployment: Development
        caches:
          - docker
        script:
          - yum install -y -q jq
          - aws configure set aws_access_key_id $development_aws_access_key_id --profile default
          - aws configure set aws_secret_access_key $development_aws_secret_access_key --profile default
          - aws configure set region eu-central-1 --profile default
          - aws configure set role_arn $development_vault_role_arn --profile vault
          - aws configure set source_profile default --profile vault
          - aws configure set region eu-central-1 --profile vault
          - aws secretsmanager get-secret-value --secret-id $development_secret_path --profile vault | jq -r ".SecretString" > secrets.yaml
          - pipe: *aws-eks-helm-deploy
            variables:
              AWS_ACCESS_KEY_ID: $development_aws_access_key_id
              AWS_SECRET_ACCESS_KEY: $development_aws_secret_access_key
              ROLE_ARN: $development_role_arn
              CLUSTER_NAME: microservices-core-development
              CHART: helm
              RELEASE_NAME: bipro-taa
              NAMESPACE: microservices
              SET: [
                'replicaCount=3',
                # # Base URLs could be passed as Bitbucket variable
                'env.insure.taa.url=http://taa.insure-core.development.andsafe/dgg-insure-general-taa',
                'env.insure.iq.url=http://iq.insure-core.development.andsafe/insure-iq-rest/headless',
                'env.biss.extern.biproSts.externalUrl=https://development.andsafe.de/api',
                'env.biss.extern.biproTaa.externalUrl=https://development.andsafe.de/api',
                # # This should be retrieved by Bitbucket Variable as soon as M-Files has been migrated to new environment
                'env.biss.extern.pnw.mFilesURL=http://m-files-core:56732',
                'env.andsafe.biproSts.cognitoClientId=5kqlr9abb2rr6dg7vqi9jsimeb',
                'env.andsafe.biproSts.cognitoPoolId=eu-central-1_hy5BvP33U',
                'env.zuers.url=http://zuers.microservices-core.development.andsafe',
                'env.camunda.url=http://camunda-prozessautomatisierung.microservices-core.development.andsafe/prozess/vertrag',
                'env.camunda.user=tec-test-camunda-prozess-vertrag-abschluss-bipro',
                'env.andsafe.aws.queue.url=https://sqs.eu-central-1.amazonaws.com/628952256187/taa-anfrage.fifo'
              ]
              VALUES: [
                secrets.yaml
              ]

    - step: &deploy-to-integration
        name: Integration
        image: amazon/aws-cli
        deployment: Integration
        trigger: manual
        script:
          - yum install -y -q jq
          - aws configure set aws_access_key_id $integration_aws_access_key_id --profile default
          - aws configure set aws_secret_access_key $integration_aws_secret_access_key --profile default
          - aws configure set region eu-central-1 --profile default
          - aws configure set role_arn $integration_vault_role_arn --profile vault
          - aws configure set source_profile default --profile vault
          - aws configure set region eu-central-1 --profile vault
          - aws secretsmanager get-secret-value --secret-id $integration_secret_path --profile vault | jq -r ".SecretString" > secrets.yaml
          - pipe: *aws-eks-helm-deploy
            variables:
              AWS_ACCESS_KEY_ID: $integration_aws_access_key_id
              AWS_SECRET_ACCESS_KEY: $integration_aws_secret_access_key
              ROLE_ARN: $integration_role_arn
              CLUSTER_NAME: microservices-core-integration
              CHART: helm
              RELEASE_NAME: bipro-taa
              NAMESPACE: microservices
              SET: [
                'replicaCount=3',
                # # Base URLs could be passed as Bitbucket variable
                'env.insure.taa.url=http://taa.insure-core.integration.andsafe/dgg-insure-general-taa',
                'env.insure.iq.url=http://iq.insure-core.integration.andsafe/insure-iq-rest/headless',
                'env.biss.extern.biproSts.externalUrl=https://integration.andsafe.de/api',
                'env.biss.extern.biproTaa.externalUrl=https://integration.andsafe.de/api',
                # # This should be retrieved by Bitbucket Variable as soon as M-Files has been migrated to new environment
                'env.biss.extern.pnw.mFilesURL=http://m-files-core:56732',
                'env.andsafe.biproSts.cognitoClientId=5nluhmpl5decdbk9lp1et4mna2',
                'env.andsafe.biproSts.cognitoPoolId=eu-central-1_7S9limM29',
                'env.zuers.url=http://zuers.microservices-core.integration.andsafe',
                'env.camunda.url=http://camunda-prozessautomatisierung.microservices-core.integration.andsafe/prozess/vertrag',
                'env.camunda.user=tec-integration-camunda-prozess-vertrag-abschluss-bipro'
              ]
              VALUES: [
                secrets.yaml
              ]

    - step: &deploy-to-production
        name: Production
        image: amazon/aws-cli
        deployment: Production
        trigger: manual
        script:
          - yum install -y -q jq
          - aws configure set aws_access_key_id $production_aws_access_key_id --profile default
          - aws configure set aws_secret_access_key $production_aws_secret_access_key --profile default
          - aws configure set region eu-central-1 --profile default
          - aws configure set role_arn $production_vault_role_arn --profile vault
          - aws configure set source_profile default --profile vault
          - aws configure set region eu-central-1 --profile vault
          - aws secretsmanager get-secret-value --secret-id $production_secret_path --profile vault | jq -r ".SecretString" > secrets.yaml
          - pipe: *aws-eks-helm-deploy
            variables:
              AWS_ACCESS_KEY_ID: $production_aws_access_key_id
              AWS_SECRET_ACCESS_KEY: $production_aws_secret_access_key
              ROLE_ARN: $production_role_arn
              CLUSTER_NAME: microservices-core-production
              CHART: helm
              RELEASE_NAME: bipro-taa
              NAMESPACE: microservices
              SET: [
                'replicaCount=3',
                # # Base URLs could be passed as Bitbucket variable
                'env.insure.taa.url=http://taa.insure-core.production.andsafe/dgg-insure-general-taa',
                'env.insure.iq.url=http://iq.insure-core.production.andsafe/insure-iq-rest/headless',
                'env.biss.extern.biproSts.externalUrl=https://andsafe.de/api',
                'env.biss.extern.biproTaa.externalUrl=https://andsafe.de/api',
                # # This should be retrieved by Bitbucket Variable as soon as M-Files has been migrated to new environment
                'env.biss.extern.pnw.mFilesURL=http://m-files-core:56732',
                'env.andsafe.biproSts.cognitoClientId=69ich1k61llkbjoui8vged8f5d',
                'env.andsafe.biproSts.cognitoPoolId=eu-central-1_wBWPputY4',
                'env.zuers.url=http://zuers.microservices-core.production.andsafe',
                'env.camunda.url=http://camunda-prozessautomatisierung.microservices-core.production.andsafe/prozess/vertrag',
                'env.camunda.user=tec-production-camunda-prozess-vertrag-abschluss-bipro'
              ]
              VALUES: [
                secrets.yaml
              ]

pipelines:
  default: &default
    - parallel:
      - step: *helm-lint
      - step: *terraform-validate
  branches:
    develop:
      - <<: *default
      - step: *deploy-to-development
      - step: *deploy-to-integration
      - step: *deploy-to-production
    integration:
      - <<: *default
      - step: *deploy-to-integration
      - step: *deploy-to-production
    master:
      - <<: *default
      - step: *deploy-to-production
