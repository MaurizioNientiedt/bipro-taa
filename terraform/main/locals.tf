locals {


  # Role which allows the application to provision resources in the platform account of its stage
  platform_role_arn = data.terraform_remote_state.cluster.outputs.applications[var.name].role_arn

  # Role which is used by Bitbucket Pipeline to access Kubernetes and the application itself while runtime
  application_role_arn = data.terraform_remote_state.cluster.outputs.service_account_roles[var.name].role_arn

  # Get the last role name without its path
  application_role_name = regex("(?P<name>[a-zA-Z0-9_-]+$)", local.application_role_arn).name

  name = trimsuffix(
  var.name,
  format("-%s", var.workspace_name)
  )

  tags = merge(
    var.tags,
    {
      TF-Workspace = var.name
    }
  )
}
