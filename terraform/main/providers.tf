provider "aws" {

  alias = "platform"

  region = "eu-central-1"

  assume_role {
    role_arn = local.platform_role_arn
  }
}

provider "aws" {
  region = "eu-central-1"

  alias = "application"

  assume_role {
    role_arn = local.application_role_arn
  }
}
