terraform {
  required_version = ">= 0.13"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.1"
    }
  }

  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "andsafe"

    workspaces {
      prefix = "bipro-taa-"
    }
  }
}
