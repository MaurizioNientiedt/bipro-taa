data "terraform_remote_state" "cluster" {
  backend = "remote"

  config = {
    organization = "andsafe"
    workspaces = {
      name = format("aws-andsafe-microservices-core-%s", var.workspace_name)
    }
  }
}